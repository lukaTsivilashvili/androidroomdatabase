package com.example.approomdatabase

import android.app.Application
import android.content.Context

class ApplicationClass:Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
    }

    companion object{
        var context:Context? = null
    }
}