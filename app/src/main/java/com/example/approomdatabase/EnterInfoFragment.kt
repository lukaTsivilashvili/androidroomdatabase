package com.example.approomdatabase

import android.os.Bundle
import android.util.Log.d
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.room.Room
import com.example.approomdatabase.databinding.FragmentEnterInfoBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class EnterInfoFragment : Fragment() {

    private lateinit var binding: FragmentEnterInfoBinding
    private val viewModel: EnterInfoViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEnterInfoBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {

        observes()

        binding.btnSave.setOnClickListener {
            viewModel.write(
                binding.etFirstName.text.toString(),
                binding.etLastName.text.toString(),
                binding.etAge.text.toString(),
                binding.etAddress.text.toString(),
                binding.etHeight.text.toString(),
                binding.etProfile.text.toString(),
                binding.etId.text.toString()
            )
        }


    }

    private fun observes() {
        viewModel.users.observe(viewLifecycleOwner, {
            d("myMessage", "${it.size}")
        })
    }

}