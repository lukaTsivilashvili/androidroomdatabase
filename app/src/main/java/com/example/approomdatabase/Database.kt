package com.example.approomdatabase

import androidx.room.Room

object Database {

    val db = Room.databaseBuilder(
        ApplicationClass.context!!,
        AppDataBase.AppDatabase::class.java, "user"
    ).build()
}