package com.example.approomdatabase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EnterInfoViewModel : ViewModel() {

    private val _users = MutableLiveData<List<UserModel>>()

    val users: LiveData<List<UserModel>> = _users

    fun read() {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _users.postValue(Database.db.userDao().getAll())
            }
        }

    }

    fun write(
        firstName: String,
        lastName: String,
        age: String,
        address: String,
        height: String,
        profile: String,
        id: String
    ) {

        val users = UserModel(firstName, lastName, age, address, height, profile, id)

        viewModelScope.launch {
            withContext(Dispatchers.IO){
                Database.db.userDao().insertAll(users)
            }
        }


    }



}